
export const truthOrDareData = [
    {
        id: 1,
        categoryName: "Mjukstart",
        truth: [
            "Vem saknar du just nu?",
            "Har du någonsin blivit förhörd?",
            "Vem sa du senast jag älskar dig till?",
            "Vem såg dig senast full?",
            "Vem ringde du senast?",
            "Varför tog ditt senaste förhållande slut?",
            "Hur mycket har du i inkomst?",
            "Vem här har störst inflytande på dig?",
            "Tror du på kärlek vid första ögonkastet?",
        ],
        dare: [
            "Avsluta ditt glas.",
            "Bytt glas med en valfri spelare.",
            "Mät upp en shot och häll den i ditt glas.",
            "Ta en shot.",
            "Ge en deltagare en obekvämt lång kram.",
            "Gurgla din dricka i 10 sekunder.",
            "Stå på händer mot en vägg.",
            "Stå i brygga i tio sekunder.",
            "Sätt dig i skräddarställning.",
            "Ge någon en fotmassage i 3 minuter.",
            "Mottag en wet willy från framröstad deltagare.",
            "Läs ditt senaste sms högt. ",
            "Utmana någon på arga leken. (1shit)",
            "Låt de andra gå igenom dina bilder i 30 sek.",
        ]
    }, {
        id: 2,
        categoryName: "Låt oss bli ovänner",
        truth: [
            "Vem i rummet skulle du minst tänka dig att byta liv med? (Vännen får int evara någon av deltagarna)",
            "Vem av dina vänner har fulast hem?",
            "Vad är det elakaste du sagt om en deltagare?",
            "Vad är det elakaste du tänkt om en deltagare?",
            "Vilken deltagare vill du helst inte krama?",
            "Har du någonsin blivit dumpad?",
            "Vem i rummet skulle du absolut inte ta med till en öde ö?",
            "Vem i rummet skulle du inte vilja ha som svärförälder?",
            "Vem här har minst inflytande på dig?",
            "Fuck marry kill, vilka väljer du i rummet?",
            "Vem här skulle du inte våga ta hem till dina föräldrar?"
        ],
        dare: [
            "Smsa en vän och säg till hen att hen borde dumpa sin partner.",
            "Låt valfri deltagare lägga en loska i ditt glas.",
            "Sök på idiot bland dina sms och läs det första som kommer upp."
        ]
    }]


export const mostLikelyData = [
    {
        id: 1,
        categoryName: "Mjukstart",
        mostLikely: [
            "Gjort slut via SMS",
            "Köpt ut åt någon",
            "Festat innan jag var myndig",
            "Fuskat på ett prov",
            "Velat få någon att se dålig ut",
            "Ljugit för mina föräldrar",
            "Varit full flera helger i rad. ",
            "Skitit i att duscha på en vecka. ",
            "Varit på rave",
            "Haft en fest som spårat ur",
            "Ljugit för en vän",
            "Sjukanmält mig för att jag var bakis",
        ]
    },
    {
        id: 2,
        categoryName: "Låt oss bli ovänner",
        mostLikely: [
            "Spridit rykten om någon här",
            "Försökt få någon här full",
            "Stulit något från en vän",
            "Lämnat någon ensam ute",
            "Glömt att swisha tillbaka",
            "Varit svår att lita på.",
            "Läckt nakenbilder ",
            "Skyllt ifrån mig på någon annan",
        ]
    }]