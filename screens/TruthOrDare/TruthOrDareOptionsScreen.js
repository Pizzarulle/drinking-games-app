import { Text, View, StyleSheet, FlatList } from "react-native";
import CategoryItem from "../../components/category/CategoryItem";
import { truthOrDareData } from "../../data/data";
import { colors } from "../../constants/colors";

//Refactor this and MostLikelyOptionsScreen into one component
const TruthOrDareOptions = () => {
    return (
        <View style={styles.screenContainer}>
            <Text style={styles.bigText}>Categories</Text>
            <FlatList
                data={truthOrDareData}
                keyExtractor={(category) => category.id}
                renderItem={({ item }) => <CategoryItem categorData={item} navigation_route={"tod_categories"} />}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        paddingLeft: 30,
        backgroundColor: colors.grey,

    },
    bigText: {
        color: "white",
        fontSize: 32,
        marginVertical: 20
    },
})
export default TruthOrDareOptions;