import { useRoute } from "@react-navigation/native";
import { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { truthOrDareData } from "../../data/data.js"


const TruthOrDareScreen = () => {

    const { params } = useRoute();

    const category = truthOrDareData.filter(item => item.id === params.categoryId)[0];
    const { dare, truth } = category;

    const [question, setQuestion] = useState(null)

    const [truthIndex, setTruthIndex] = useState(0)
    const [dareIndex, setDareIndex] = useState(0)

    const onClickTruth = () => {
        if (truthIndex < truth.length) {
            setTruthIndex(truthIndex + 1)
            setQuestion(truth[truthIndex])
        } else {
            setQuestion("No more truths")
        }
    }

    const onClickDare = () => {
        if (dareIndex < dare.length) {
            setDareIndex(dareIndex + 1)
            setQuestion(dare[dareIndex])
        } else {
            setQuestion("No more dares")
        }
    }

    return (
        <View style={styles.screenContainer}>
            <View style={styles.questionContainer}>
                <Text style={styles.questionText}>{question !== null ? question : "Pick one!"}</Text>
            </View>

            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={onClickTruth} >
                    <Text style={{ ...styles.button, backgroundColor: "blue" }}>Truth</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={onClickDare}>
                    <Text style={{ ...styles.button, backgroundColor: "green" }}>Dare</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    screenContainer: {
        display: "flex",
        flex: 1,
        padding: 30,
        flexDirection: "column",
        justifyContent: "space-between",
        marginVertical: 40
    },
    questionContainer: {
        alignItems: 'center',
    },
    questionText: {
        fontSize: 24,
    },
    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    button: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        borderWidth: 1,
        borderColor: "grey",
        color: "white",
        fontSize: 18,
        fontWeight: "bold",
        borderRadius: 30,
    }
})
export default TruthOrDareScreen;