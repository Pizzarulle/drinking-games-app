import { useNavigation } from "@react-navigation/native";
import { View, StyleSheet } from "react-native";
import Card from "../components/card/Card";
import { colors } from "../constants/colors";

const StartScreen = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={styles.column}>
                <Card
                    text={"Sanning eller konka"}
                    cardColor={colors.indigo}
                    navigation_route={"truthOrDare"}
                />
                <Card
                    text={"Jag har aldrig"}
                    cardColor={colors.pink}
                    navigation_route={"mostLikely"}
                />
            </View>

            <View style={styles.column}>
                <Card
                    text={"Pekleken"}
                    cardColor={colors.orange}
                />
                <Card
                    text={"Charader"}
                    cardColor={colors.green}
                />
            </View>
        </View >
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: colors.grey
    },
    column: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
    },

})
export default StartScreen;