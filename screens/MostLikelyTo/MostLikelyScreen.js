import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { mostLikelyData } from "../../data/data.js"
import { useRoute } from "@react-navigation/native";
import { useState } from "react";

const MostLikelyScreen = () => {

    const { params } = useRoute();
    const category = mostLikelyData.filter(item => item.id === params.categoryId)[0];

    const { mostLikely } = category;

    const [question, setQuestion] = useState(null)
    const [index, setIndex] = useState(0)

    const onClickButton = () => {
        if (index < mostLikely.length) {
            setIndex(index + 1)
            setQuestion(mostLikely[index])
        } else {
            setQuestion("No more questions")
        }
    }
    return (
        <View style={styles.screenContainer}>
            <View style={styles.questionContainer}>
                <Text style={styles.questionText}>{question !== null ? question : "Click on start!"}</Text>
            </View>

            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={onClickButton} >
                    <Text style={{ ...styles.button, backgroundColor: "blue" }}>{index === 0 ? "Start" : "Next"}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    screenContainer: {
        display: "flex",
        flex: 1,
        padding: 30,
        flexDirection: "column",
        justifyContent: "space-between",
        marginVertical: 40
    },
    questionContainer: {
        alignItems: 'center',
    },
    questionText: {
        fontSize: 24,
    },
    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    button: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        borderWidth: 1,
        borderColor: "grey",
        color: "white",
        fontSize: 18,
        fontWeight: "bold",
        borderRadius: 30,
    }
})

export default MostLikelyScreen;