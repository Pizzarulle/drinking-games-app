export const colors = {
    pink: "#f50057",
    purple: "#d500f9",
    indigo: "#3d5afe",
    lightBlue: "#00b0ff",
    green: "#00e676",
    lime: "#c6ff00",
    orange: "#ff9100",
    red: "#ff3d00",
    grey: "#212121"
}

export const randomColor = () => {
    const keys = Object.keys(colors).filter(key => key !== "grey");
    return colors[keys[keys.length * Math.random() << 0]];
}