import StartScreen from './screens/StartScreen';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TruthOrDareScreen from './screens/TruthOrDare/TruthOrDareScreen';
import TruthOrDareOptionsScreen from './screens/TruthOrDare/TruthOrDareOptionsScreen';
import MostLikelyOptionsScreen from './screens/MostLikelyTo/MostLikelyOptionsScreen';
import MostLikelyScreen from './screens/MostLikelyTo/MostLikelyScreen';


export default function App() {
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="start" component={StartScreen} options={{ title: "Games" }}/>

        <Stack.Screen name="truthOrDare" component={TruthOrDareOptionsScreen} options={{ title: "Truth Or Dare" }}/>
        <Stack.Screen name="tod_categories" component={TruthOrDareScreen}  options={({ route }) => ({ title: route.params.title })}/>

        <Stack.Screen name="mostLikely" component={MostLikelyOptionsScreen} options={{ title: "Most Likely To" }}/>
        <Stack.Screen name="ml_categories" component={MostLikelyScreen}  options={({ route }) => ({ title: route.params.title })}/>

      </Stack.Navigator>
    </NavigationContainer>
  );
}

