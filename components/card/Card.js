import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

const Card = ({ text, navigation_route, cardColor }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={() => navigation_route && navigation.navigate(navigation_route)}>
            <View style={{ ...styles.cardContainer, backgroundColor: cardColor, borderColor: cardColor }}>
                <Text style={styles.text}>{text}</Text>
                {!navigation_route && <Text style={styles.infoText}>Coming soon...</Text>}
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    cardContainer: {
        minWidth: "90%",
        height: 100,
        padding:10,
        marginVertical: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        borderWidth: 3,
    },
    text: {
        textAlign: "center",
        color: "white",
        fontSize: 22,
        fontWeight: "700"
    },
})

export default Card;