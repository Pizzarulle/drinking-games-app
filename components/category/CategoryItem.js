import React from 'react';
import { Text, StyleSheet } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { randomColor } from '../../constants/colors';

const CategoryItem = ({ categorData, navigation_route }) => {

    const navigation = useNavigation();

    const { categoryName, id, } = categorData
    const categoryColor = randomColor();

    return (
        <Text
            style={{ ...styles.text, borderBottomColor: categoryColor }}
            onPress={() => navigation.navigate(navigation_route, { title: categoryName, categoryId: id, color: categoryColor })}
        >{categoryName}</Text>
    );
}

const styles = StyleSheet.create({
    text: {
        fontSize: 18,
        color: "white",
        borderBottomWidth: 2,
        alignSelf: "flex-start",
        marginVertical: 3,
        marginLeft: 20
    }
})

export default CategoryItem;